# Basic Node Commands and Tools

## Node
Node is Javascript runtime 
It is being used for running any Javascript based 
code.

## NPM
NPM = node package manager 
it is being used for installing / removing / updating node packages

## NPX 
It is a tool used for executing the node stand alone packages. 

<i> Note: node packages can be either a simple library or a framework or a stand along tool. </i>

## Creating the first project

```bash
npm init # create an empty generic node project
```

## Create a react project
```bash
npm i -g create-react-app # install the tool create-react-app globally. So, we can call it from any where. This is one-time step.

npx create-react-app app-name # create a new complete react app with all the needed tools and stating code.
```

## Running the project
```bash
# node index.js
# this is manual way of running the project 

npm start # where npm starts the needed tools and then runs our code.
```