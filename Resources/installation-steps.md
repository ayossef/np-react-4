# React Env. Installation Guide

## Install Node

In order to install node
1. Install Node using the Ubuntu package (Easy and simple -- it doesn't contain the most recent versions of Node)
2. Install node via the node respositories -- recommended 
   https://github.com/nodesource/distributions

## Install VS Code 
Simply, by downloading the package and installing it 

## Install required VS Code Plugins
That contains the snippets, which makes our coding faster

## Install create-react-app tool

```bash
npm install -g create-react-app 
```

## Create the first project
```bash
npx create-react-app app-name
```