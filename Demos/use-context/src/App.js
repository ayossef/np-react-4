import logo from './logo.svg';
import './App.css';
import MainComp from './components/MainComp';
import AnotherComp from './components/AnotherComp';
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <MainComp></MainComp>
        <h5>Adding Another Comp to App</h5>
        <AnotherComp></AnotherComp>
      </header>
    </div>
  );
}

export default App;
