import React from "react";
import {createContext, useContext, useState} from "react";
import AnotherComp from "./AnotherComp";

export const mainContext = createContext();

function MainComp() {
    const [counterState, setcounterState] = useState(0)
    const msg = 'Context Hooks are awesome'
    const localStore = {
        msg: 'Context Hooks are awesome',
        counter: counterState

    }
  return (
   <mainContext.Provider value={localStore}>
     <div>
      <h2>MainComp</h2>
      <h3>{msg}</h3>
      <SecondaryComp></SecondaryComp>
      <h5>Adding Another Comp to MainComp</h5>
        <AnotherComp></AnotherComp>
    </div>
   </mainContext.Provider>
  );
}

export default MainComp;


export const SecondaryComp = () => {
   const localStore = useContext(mainContext)
    return  (
        <div>
            <h2>
                Secondary Component
            </h2>
            <h3>
                {localStore.msg}
            </h3>
        </div>
    )
}