import React from 'react'
import { mainContext } from './MainComp'
import {useContext} from 'react';
function AnotherComp() {
    const localStore = useContext(mainContext)
  return (
    <div>AnotherComp
        <h4>
            Message from Context is {localStore.msg}
        </h4>
    </div>
  )
}

export default AnotherComp