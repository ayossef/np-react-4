import logo from './logo.svg';
import './App.css';

function App() {
  var message = "Welcome to React Demo"
  var list = ["Tea","Coffee","Water"]
  return ( // JSX
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          New Vesion of the App Component is here.
        </p>
        <img src={logo} className="App-logo" alt="logo" />
        <p>{message}</p>
        <p>{100 + 100}</p>
        <p>{list.map((item)=>item)}</p>
      </header>
    </div>
  );
}

export default App;
