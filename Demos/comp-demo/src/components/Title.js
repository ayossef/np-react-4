import React from 'react'
import { useState, useEffect } from 'react'

function Title() {
    const list = ["Good Morning ","Welcome", "Hello", "Hi"]
    const [index, setIndex] = useState(0)
    useEffect(() => {
        console.log("New Effect")
    }, [] )
    
    const sayThanks = () => {
        console.log("Thanks")
        setIndex(index +1);
        console.log(list[index%list.length])
    }
    return (
        <div>
            <p>
            {list[index%list.length]} to our React App
            </p>
            <button onClick={sayThanks}>Thanks</button>
        </div>
    )
}

export default Title