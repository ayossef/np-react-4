import PropTypes from 'prop-types'
import React, { Component } from 'react'

export default class Counter extends Component {
    constructor(props){
        super(props)

        this.state = {
            countValue : 0
        }
    }
    componentDidMount(){
        console.log("Component is Mounted")
    }

    updateCount = ()=> {
      this.setState({
        countValue : this.state.countValue + 1 
      })
      console.log(this.state.countValue)
    }
    render() {
        return (
            <div><p>
                {this.state.countValue}
                </p>
                <button onClick={this.updateCount}>
                    Count</button></div>
        )
    }
}
