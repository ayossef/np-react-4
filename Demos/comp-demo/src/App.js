import logo from './logo.svg';
import './App.css';
import Title from './components/Title';
import Counter from './components/Counter';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Title></Title>
        <Counter></Counter>
      </header>
    </div>
  );
}

export default App;
