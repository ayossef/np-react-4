import React, { Component } from 'react'

export default class RBGame extends Component {
    bills =['Red', 'Blue']
    currentGuess = ''
    constructor(){
      super()
      this.state = {
        result:'No Result yet'
      }
    }
    newGame = () => {
        let randomNumber = Math.round(Math.random())
        console.log(randomNumber)
        this.currentGuess = this.bills[randomNumber]
    }
    blueGuess = () => {
        if(this.currentGuess === 'Blue'){
         this.setState({result: 'Congrats'})
        }else{
         this.setState({result: 'try harder'})
        }
    }
    redGuess = () => {
        if(this.currentGuess === 'Red'){
          this.setState({result: 'Congrats'})
        }else{
         this.setState({result: 'try harder'})
        }
    }
  render() {
    return (
      <div>
        <h4>Play here</h4>
        <button onClick={this.newGame}> New Game </button>
        <br></br>
        <button onClick={this.redGuess}> Red Guess </button>
        <button onClick={this.blueGuess}> Blue Guess</button>
        <h4>Result is {this.state.result}</h4>
      </div>
    )
  }
}
