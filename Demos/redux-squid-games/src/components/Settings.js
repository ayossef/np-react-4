import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { reset } from '../redux/hsSlice'
import { resetScore } from '../redux/score'
const Settings = (props) => {
    const highestScoreFromRedux = useSelector((state)=> state.highScore.value)
    // obtain the dispatch Q by calling useDispactch
    const dispatchQ = useDispatch()
    const resetAllScores = () => {
        // send the reset action to the dispatch Q
        dispatchQ(reset())
        dispatchQ(resetScore())
    }
  return (
    <div>Settings
        <p> Highest Score is {highestScoreFromRedux}</p>
        <button onClick={resetAllScores}> Reset Scores </button>
    </div>
  )
}

export default Settings