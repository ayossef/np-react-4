import React from 'react'
import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { scoreInc, won } from '../redux/score'
import { setHighScore } from '../redux/hsSlice'
import Auth from './Auth'
const GuessNumber = (props) => {
    const highScore = useSelector((state)=> state.highScore.value)
    const wonFlag = useSelector((state)=> state.score.won)
    const [result, setResult] = useState('not yet')
    const [userInput, setUserInput] = useState('')
   // const [won, setWon] = useState(false)
    const dispatchQ = useDispatch()
    const score = useSelector((state)=> state.score.value)
    console.log('Rendered with:'+ props.num)
    const luckyGuess = () => {
        console.log(props.num)
        console.log(userInput)
        if (props.num === userInput) {
            setResult('Congratulations ..')
            dispatchQ(won())
            if(score >= highScore){
               dispatchQ(setHighScore({newHighScore: score+1}))
            }
            dispatchQ(scoreInc())
        } else if(props.num < userInput){
            setResult('Go Lower ..')
        }else {
            setResult('Go Higher')
        }
    }
    return (
        <div>
           <h4>Guess Number Game</h4>
            <input placeholder='Your guess here'
                value={userInput}
                onChange={(event) => {
                    setUserInput(Number(event.target.value))
                }}>
            </input>
            {!wonFlag && <button onClick={luckyGuess}> Feeling lucky ..</button>}
            <h4>Result {result}</h4>
            <h3> your current score is {score}</h3>
            <h4>High Score is {highScore}</h4>
        </div>
    )
}
export default GuessNumber