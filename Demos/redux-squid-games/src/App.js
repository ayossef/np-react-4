import './App.css';
import CoverImg from './components/CoverImg';
import Header from './components/Header';
import GuessNumber from './components/GuessNumber';
import { useState } from 'react';
import Settings from './components/Settings';
import { useDispatch, useSelector } from 'react-redux';
import { resetScore, resetWinFlag } from './redux/score';
import Auth from './components/Auth';
function App() {
  const [luckyNum, setLuckyNum] = useState(0) 
  const [highestScore, setHighestScore] = useState(2)
  const dispatchQ = useDispatch()
  const newGame = () => {
   
    setLuckyNum(Math.round(Math.random()*10))
    dispatchQ(resetScore())
    dispatchQ(resetWinFlag())

  }
  return (
    <div className="App">
     <Header></Header>
     <CoverImg ciw={400}></CoverImg>
     <Auth></Auth>
     
    <button onClick={newGame}> New Game </button>
    <GuessNumber num = {luckyNum}></GuessNumber>
    <Settings hs = {highestScore}></Settings>
    </div>
  );
}

export default App;
