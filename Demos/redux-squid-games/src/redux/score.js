import { createSlice } from "@reduxjs/toolkit";

export const score = createSlice({
    name:'score',
    initialState:{
        value: 0,
        won: false
    },
    reducers:{
        setScore: (state, action) =>{state.value = action.payload.newScore},
        scoreInc: (state) => {state.value = state.value + 1},
        resetScore: (state) => {state.value = 0},
        won: (state) => {state.won = true},
        resetWinFlag: (state) => {state.won = false}
    }
})

export const {setScore, resetScore, scoreInc, won, resetWinFlag} = score.actions
export default score.reducer