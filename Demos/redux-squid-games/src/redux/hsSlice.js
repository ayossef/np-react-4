import { createSlice } from "@reduxjs/toolkit";

export const hsSlice = createSlice({
    name:'highScore',
    initialState:{
        value:0
    },
    reducers:{
        reset: (state) => {state.value = 0},
        setHighScore: (state, action) => {state.value = action.payload.newHighScore}
    }
})
export const {reset, setHighScore} = hsSlice.actions

export default hsSlice.reducer