import { createSlice } from "@reduxjs/toolkit";

export const wonSlice = createSlice({
    name:'won',
    initialState:{
        value: false
    },
    reducers:{
        won: (state) => {state.value = true}
    }
})

export const {won} = wonSlice.actions
export default wonSlice.reducer