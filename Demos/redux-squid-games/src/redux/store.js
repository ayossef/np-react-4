import {combineReducers, configureStore} from '@reduxjs/toolkit'
import hsSlice  from './hsSlice';
import score from './score';
import won from './won';

let reducer = combineReducers({
    highScore: hsSlice,
    score: score,
    won: won
})
export default configureStore({
    reducer
})