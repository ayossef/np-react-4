import React from 'react'
import { useFetch } from '../loader/useFetch'

function Brands() {
    const brandsUrl = "http://localhost:3000/brands"
    const brandsList = useFetch(brandsUrl)
  return (
    <div><h3>
        Brands</h3>
        {brandsList && 
      brandsList.map(
        (brand) => 
      <p> {brand.id}-  {brand.name}</p>)}
        </div>
  )
}

export default Brands