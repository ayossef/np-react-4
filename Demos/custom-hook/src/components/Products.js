import React from "react";
import { useFetch } from "../loader/useFetch";

function Products() {
  const productsList = useFetch("http://localhost:3000/products");
  return (
    <div>
      <h3>Products</h3>
      {productsList && 
      productsList.map(
        (product) => 
      <p> {product.id}-  {product.name} with price {product.price}</p>)}
    </div>
  );
}

export default Products;
