import logo from './logo.svg';
import './App.css';
import Products from './components/Products';
import Brands from './components/Brands';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
       <Products></Products>
       <Brands></Brands>
      </header>
    </div>
  );
}

export default App;
