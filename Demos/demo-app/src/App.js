import './App.css';
import CoverImg from './components/CoverImg';
import Header from './components/Header';
import GuessNumber from './components/GuessNumber';
import { useState } from 'react';

function App() {
  const [luckyNum, setLuckyNum] = useState(0) 
  
  const newGame = () => {
    console.log(luckyNum)
    setLuckyNum(Math.round(Math.random()*10))
    console.log(luckyNum)
  }
  return (
    <div className="App">
     <Header></Header>
     <CoverImg ciw={400}></CoverImg>
    <button onClick={newGame}> New Game </button>
    <GuessNumber num = {luckyNum}></GuessNumber>
    </div>
  );
}

export default App;
