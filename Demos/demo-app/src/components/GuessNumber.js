import React from 'react'
import { useState } from 'react'

const GuessNumber = (props) => {
    const [result, setResult] = useState('not yet')
    const [userInput, setUserInput] = useState('')
    console.log('Rendered with:'+ props.num)
    const luckyGuess = () => {
        console.log(props.num)
        console.log(userInput)
        if (props.num === userInput) {
            setResult('Congratulations ..')
        } else if(props.num < userInput){
            setResult('Go Lower ..')
        }else {
            setResult('Go Higher')
        }
    }
    return (
        <div>
            <input placeholder='Your guess here'
                value={userInput}
                onChange={(event) => {
                    setUserInput(Number(event.target.value))
                }}>
            </input>
            <button onClick={luckyGuess}> Feeling lucky ..</button>
            <h4>Result {result}</h4>
        </div>
    )
}
export default GuessNumber