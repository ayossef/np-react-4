import React, { Component } from 'react'

export default class Game extends Component {
    messages = ['Game has started', 'Playing ..', ' it is on..']
    currentMsgIndex = 0
    play = ()=>{
        this.currentMsgIndex +=1 
        this.currentMsgIndex = this.currentMsgIndex%3
        console.log(this.messages[this.currentMsgIndex])
    }
  render() {
    return (
      <div>
        <h5>Play the Game</h5>
        <button onClick={this.play}>Play</button>
      </div>
    )
  }
}
